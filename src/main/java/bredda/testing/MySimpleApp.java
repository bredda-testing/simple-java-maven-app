package bredda.testing;

/**
 * Hello world!
 */
public class MySimpleApp
{

    private final String message = "Hello World!";

    public MySimpleApp() {}

    public static void main(String[] args) {
        System.out.println(new MySimpleApp().getMessage());
    }

    private final String getMessage() {
        return message;
    }

}
